﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer
{
    public interface ISubjectRepository
    {
        void GetConnectionString(string connString);
        List<Subject> GetAllSubjects();
    }
}
