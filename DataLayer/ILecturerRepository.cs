﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer
{
    public interface ILecturerRepository
    {
        void GetConnectionString(string connString);
        List<Lecturer> GetAllLecturers();

        List<LecturerJoin> GetAllLecturersJoin();
        int InsertLecturer(Lecturer l);
    }
}
