﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DataLayer
{
    public class ClassroomRepository : IClassroomRepository
    {
        public string ConnectionString;
        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }
        public List<Classroom> GetAllClassrooms()
        {
            List<Classroom> listToReturn = new List<Classroom>();

            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM Classroom";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Classroom c = new Classroom();
                    c.id = dataReader.GetInt32(0);
                    c.number = dataReader.GetInt32(1);
                    c.type = dataReader.GetString(2);
                    c.capacity = dataReader.GetInt32(3);
                    c.floor = dataReader.GetInt32(4);
                    c.fullHDProjector = dataReader.GetBoolean(5);

                    listToReturn.Add(c);
                }
            }
            return listToReturn;
        }
    }
}
