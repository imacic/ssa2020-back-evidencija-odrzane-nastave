﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.Models;

namespace DataLayer
{
    public interface ITeachingRecordsRepository
    {
        void GetConnectionString(string connString);
        List<TeachingRecords> GetAllTeachingRecords();
        List<TeachingRecordsJoin> GetAllTeachingRecordsJoin();
        List<TeachingRecordsUpdate> GetAllTeachingRecordsUpdate();
        int InsertTeachingRecord(TeachingRecords tr);
        int UpdateTeachingRecord(TeachingRecords tr);
        int DeleteTeachingRecord(int id);
    }
}
