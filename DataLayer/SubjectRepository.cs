﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DataLayer.Models;

namespace DataLayer
{
    public class SubjectRepository : ISubjectRepository
    {
        public string ConnectionString;
        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }
        public List<Subject> GetAllSubjects()
        {
            List<Subject> listToReturn = new List<Subject>();

            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM Subject";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Subject s = new Subject();
                    s.id = dataReader.GetInt32(0);
                    s.name = dataReader.GetString(1);
                    s.departmentID = dataReader.GetInt32(2);

                    listToReturn.Add(s);
                }
            }
            return listToReturn;
        }

    }
}
