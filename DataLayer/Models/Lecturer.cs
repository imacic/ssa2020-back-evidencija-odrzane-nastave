﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class Lecturer
    {
        [Key]
        public int id { get; set; }
        public int departmentID { get; set; }
        public string departmentName { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string status { get; set; }

        public Lecturer(int id, int departmentID, string name, string surname, string username, string password, string status)
        {
            this.id = id;
            this.departmentID = departmentID;
            this.name = name;
            this.surname = surname;
            this.username = username;
            this.password = password;
            this.status = status;
        }

        public Lecturer()
        {

        }
    }
}
