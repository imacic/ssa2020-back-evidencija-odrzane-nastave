﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    public class Department
    {
        public int id { get; set; }
        public string name { get; set; }

        public Department(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public Department()
        {

        }
    }
}
