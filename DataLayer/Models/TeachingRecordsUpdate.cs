﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    public class TeachingRecordsUpdate
    {
        public int id { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string type { get; set; }
        public bool maintained { get; set; }
        public int numberOfClasses { get; set; }
        public int subjectID { get; set; }
        public int lecturerID { get; set; }
        public string lecturerName { get; set; }
        public string lecturerSurname { get; set; }
        public int departmentID { get; set; }
        public string departmentName { get; set; }
        public int classroomID { get; set; }
        public string comment { get; set; }

        public TeachingRecordsUpdate(int id, string date, string time, string type, bool maintained, int numberOfClasses, int subjectID, int lecturerID, string lecturerName, string lecturerSurname, int departmentID, string departmentName, int classroomID, string comment)
        {
            this.id = id;
            this.date = date;
            this.time = time;
            this.type = type;
            this.maintained = maintained;
            this.numberOfClasses = numberOfClasses;
            this.subjectID = subjectID;
            this.lecturerID = lecturerID;
            this.lecturerName = lecturerName;
            this.lecturerSurname = lecturerSurname;
            this.departmentID = departmentID;
            this.departmentName = departmentName;
            this.classroomID = classroomID;
            this.comment = comment;
        }

        public TeachingRecordsUpdate()
        {
        }
    }

}
