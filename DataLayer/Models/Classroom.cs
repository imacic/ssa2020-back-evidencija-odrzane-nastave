﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    public class Classroom
    {
        public int id { get; set; }
        public int number { get; set; }
        public string type { get; set; }
        public int capacity { get; set; }
        public int floor { get; set; }
        public bool fullHDProjector { get; set; }

        public Classroom(int id, int number, string type, int capacity, int floor, bool fullHDProjector)
        {
            this.id = id;
            this.number = number;
            this.type = type;
            this.capacity = capacity;
            this.floor = floor;
            this.fullHDProjector = fullHDProjector;
        }

        public Classroom()
        {

        }
    }
}
