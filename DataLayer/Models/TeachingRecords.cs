﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    public class TeachingRecords
    {
        public int id { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string type { get; set; }
        public bool maintained { get; set; }
        public int numberOfClasses { get; set; }
        public int subjectID { get; set; }
        public int lecturerID { get; set; }
        public int classroomID { get; set; }
        public string comment { get; set; }

        public TeachingRecords(int id, string date, string time, string type, bool maintained, int numberOfClasses, int subjectID, int lecturerID, int classroomID, string comment)
        {
            this.id = id;
            this.date = date;
            this.time = time;
            this.type = type;
            this.maintained = maintained;
            this.numberOfClasses = numberOfClasses;
            this.subjectID = subjectID;
            this.lecturerID = lecturerID;
            this.classroomID = classroomID;
            this.comment = comment;
        }

        public TeachingRecords()
        {

        }
    }
}
