﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    public class LecturerJoin
    {
        public int id { get; set; }
        public int departmentID { get; set; }
        public string fullName { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string status { get; set; }
        public string departmentName { get; set; }

        public LecturerJoin(int id, int departmentID, string fullName, string username, string password, string status, string departmentName)
        {
            this.id = id;
            this.departmentID = departmentID;
            this.fullName = fullName;
            this.username = username;
            this.password = password;
            this.status = status;
            this.departmentName = departmentName;
        }

        public LecturerJoin()
        {

        }

    }
}
