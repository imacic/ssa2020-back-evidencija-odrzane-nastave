﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    public class Subject
    {
        public int id { get; set; }
        public int departmentID { get; set; }
        public string name { get; set; }

        public Subject(int id, int departmentID, string name)
        {
            this.id = id;
            this.departmentID = departmentID;
            this.name = name;
        }

        public Subject()
        {

        }
    }
}
