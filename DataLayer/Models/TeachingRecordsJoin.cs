﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    public class TeachingRecordsJoin
    {
        public int id { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string type { get; set; }
        public bool maintained { get; set; }
        public int numberOfClasses { get; set; }
        public string subjectName { get; set; }
        public int lecturerID { get; set; }
        public string lecturerFullname { get; set; }
        public string departmentName { get; set; }
        public int classroomNumber { get; set; }
        public string comment { get; set; }

        public TeachingRecordsJoin(int id, string date, string time, string type, bool maintained, int numberOfClasses, string subjectName, int lecturerID, string lecturerFullname, int classroomNumber, string comment)
        {
            this.id = id;
            this.date = date;
            this.time = time;
            this.type = type;
            this.maintained = maintained;
            this.numberOfClasses = numberOfClasses;
            this.subjectName = subjectName;
            this.lecturerID = lecturerID;
            this.lecturerFullname = lecturerFullname;
            this.classroomNumber = classroomNumber;
            this.comment = comment;
        }

        public TeachingRecordsJoin()
        {

        }
    }
}
