﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer
{
    public interface IDepartmentRepository
    {
        void GetConnectionString(string connString);
        List<Department> GetAllDepartments();
    }
}
