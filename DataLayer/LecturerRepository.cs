﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DataLayer.Models;

namespace DataLayer
{
    public class LecturerRepository : ILecturerRepository
    {
        public string ConnectionString;
        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }
        public List<Lecturer> GetAllLecturers()
        {
            List<Lecturer> listToReturn = new List<Lecturer>();

            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM Lecturer";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Lecturer l = new Lecturer();
                    l.id = dataReader.GetInt32(0);
                    l.departmentID = dataReader.GetInt32(1);
                    l.name = dataReader.GetString(2);
                    l.surname = dataReader.GetString(3);
                    l.username = dataReader.GetString(4);
                    l.password = dataReader.GetString(5);
                    l.status = dataReader.GetString(6);

                    listToReturn.Add(l);
                }

            }
            return listToReturn;
        }

        public List<LecturerJoin> GetAllLecturersJoin()
        {
            List<LecturerJoin> listToReturn = new List<LecturerJoin>();

            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT Lecturer.ID, Lecturer.DepartmentID, CONCAT(Lecturer.Name,' ',Lecturer.Surname) AS fullName, Lecturer.Username, Lecturer.Password, Lecturer.Status, Department.Name FROM Lecturer JOIN Department ON Lecturer.DepartmentID = Department.ID";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    LecturerJoin l = new LecturerJoin();
                    l.id = dataReader.GetInt32(0);
                    l.departmentID = dataReader.GetInt32(1);
                    l.fullName = dataReader.GetString(2);
                    l.username = dataReader.GetString(3);
                    l.password = dataReader.GetString(4);
                    l.status = dataReader.GetString(5);
                    l.departmentName = dataReader.GetString(6);

                    listToReturn.Add(l);
                }

            }
            return listToReturn;
        }

        public int InsertLecturer(Lecturer l)
        {
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                List<Lecturer> lista = GetAllLecturers();

                if (string.IsNullOrWhiteSpace(l.password))
                    throw new ApplicationException("Password is required");
                for (int i = 0; i < lista.Count; i++)
                {
                    if(lista[i].username == l.username)
                    {
                        throw new ApplicationException("Username " + l.username + " is already taken");
                    }
                }

                    dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = $"INSERT INTO dbo.Lecturer(DepartmentID,Name,Surname,Username,Password,Status) VALUES('{l.departmentID}', '{l.name}', '{l.surname}', '{l.username}', '{l.password}', '{l.status}')";

                return command.ExecuteNonQuery();
            }
        }
    }
}
