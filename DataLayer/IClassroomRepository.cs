﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer
{
    public interface IClassroomRepository
    {
        void GetConnectionString(string connString);
        List<Classroom> GetAllClassrooms();
    }
}
