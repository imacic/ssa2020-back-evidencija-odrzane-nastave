﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DataLayer.Models;

namespace DataLayer
{
    public class TeachingRecordsRepository: ITeachingRecordsRepository
    {
        public string ConnectionString;
        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }
        public List<TeachingRecords> GetAllTeachingRecords()
        {
            List<TeachingRecords> listToReturn = new List<TeachingRecords>();

            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM TeachingRecords";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    TeachingRecords tr = new TeachingRecords();
                    tr.id = dataReader.GetInt32(0);
                    tr.date = dataReader.GetString(1);
                    tr.time = dataReader.GetString(2);
                    tr.type = dataReader.GetString(3);
                    tr.maintained = dataReader.GetBoolean(4);
                    tr.numberOfClasses = dataReader.GetInt32(5);
                    tr.subjectID = dataReader.GetInt32(6);
                    tr.lecturerID = dataReader.GetInt32(7);
                    tr.classroomID = dataReader.GetInt32(8);
                    tr.comment = dataReader.GetString(9);
           

                    listToReturn.Add(tr);
                }
            }
            return listToReturn;
        }
        public List<TeachingRecordsJoin> GetAllTeachingRecordsJoin()
        {
            List<TeachingRecordsJoin> listToReturn = new List<TeachingRecordsJoin>();

            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT TeachingRecords.ID, TeachingRecords.Date, TeachingRecords.Time, TeachingRecords.Type, TeachingRecords.Maintained, TeachingRecords.NumberOfClasses, Subject.Name, TeachingRecords.LecturerID, CONCAT(Lecturer.Name,' ',Lecturer.Surname) AS lecturerFullname, Department.Name, Classroom.Number, TeachingRecords.Comment FROM TeachingRecords JOIN Subject ON TeachingRecords.SubjectID = Subject.ID JOIN Lecturer ON TeachingRecords.LecturerID = Lecturer.ID JOIN Classroom ON TeachingRecords.ClassroomID = Classroom.ID JOIN Department ON Lecturer.DepartmentID = Department.ID";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    TeachingRecordsJoin tr = new TeachingRecordsJoin();
                    tr.id = dataReader.GetInt32(0);
                    tr.date = dataReader.GetString(1);
                    tr.time = dataReader.GetString(2);
                    tr.type = dataReader.GetString(3);
                    tr.maintained = dataReader.GetBoolean(4);
                    tr.numberOfClasses = dataReader.GetInt32(5);
                    tr.subjectName = dataReader.GetString(6);
                    tr.lecturerID = dataReader.GetInt32(7);
                    tr.lecturerFullname = dataReader.GetString(8);
                    tr.departmentName = dataReader.GetString(9);
                    tr.classroomNumber = dataReader.GetInt32(10);
                    tr.comment = dataReader.GetString(11);


                    listToReturn.Add(tr);
                }
            }
            return listToReturn;
        }

        public int InsertTeachingRecord(TeachingRecords tr)
        {
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = $"INSERT INTO dbo.TeachingRecords(Date,Time,Type,Maintained,NumberOfClasses,SubjectID,LecturerID,ClassroomID,Comment) VALUES('{tr.date}', '{tr.time}', '{tr.type}', '{tr.maintained}', '{tr.numberOfClasses}', '{tr.subjectID}', '{tr.lecturerID}', '{tr.classroomID}', '{tr.comment}')";

                return command.ExecuteNonQuery();
            }
        }

        public int UpdateTeachingRecord(TeachingRecords tr)
        {
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = $"UPDATE dbo.TeachingRecords SET Date= '{tr.date}', Time= '{tr.time}', Type= '{tr.type}', Maintained= '{tr.maintained}', NumberOfClasses= '{tr.numberOfClasses}', SubjectID= '{tr.subjectID}', LecturerID= '{tr.lecturerID}', ClassroomID= '{tr.classroomID}', Comment= '{tr.comment}' WHERE ID = '{tr.id}'";

                return command.ExecuteNonQuery();
            }
        }

        public int DeleteTeachingRecord(int id)
        {
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "DELETE FROM dbo.TeachingRecords WHERE ID = " + id;

                return command.ExecuteNonQuery();
            }
        }

        public List<TeachingRecordsUpdate> GetAllTeachingRecordsUpdate()
        {
            List<TeachingRecordsUpdate> listToReturn = new List<TeachingRecordsUpdate>();

            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT TeachingRecords.ID, TeachingRecords.Date, TeachingRecords.Time, TeachingRecords.Type, TeachingRecords.Maintained, TeachingRecords.NumberOfClasses, TeachingRecords.SubjectID, TeachingRecords.LecturerID, Lecturer.Name, Lecturer.Surname, Lecturer.DepartmentID, Department.Name, TeachingRecords.ClassroomID, TeachingRecords.Comment FROM TeachingRecords JOIN Lecturer ON TeachingRecords.LecturerID = Lecturer.ID JOIN Department ON Lecturer.DepartmentID = Department.ID";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    TeachingRecordsUpdate tr = new TeachingRecordsUpdate();
                    tr.id = dataReader.GetInt32(0);
                    tr.date = dataReader.GetString(1);
                    tr.time = dataReader.GetString(2);
                    tr.type = dataReader.GetString(3);
                    tr.maintained = dataReader.GetBoolean(4);
                    tr.numberOfClasses = dataReader.GetInt32(5);
                    tr.subjectID = dataReader.GetInt32(6);
                    tr.lecturerID = dataReader.GetInt32(7);
                    tr.lecturerName = dataReader.GetString(8);
                    tr.lecturerSurname = dataReader.GetString(9);
                    tr.departmentID = dataReader.GetInt32(10);
                    tr.departmentName = dataReader.GetString(11);
                    tr.classroomID = dataReader.GetInt32(12);
                    tr.comment = dataReader.GetString(13);


                    listToReturn.Add(tr);
                }
            }
            return listToReturn;
        }
    }
}
