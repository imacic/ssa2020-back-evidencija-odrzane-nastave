﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DataLayer.Models;

namespace DataLayer
{
    public class DepartmentRepository : IDepartmentRepository
    {
        public string ConnectionString;
        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }
        public List<Department> GetAllDepartments()
        {
            List<Department> listToReturn = new List<Department>();

            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM Department";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Department d = new Department();
                    d.id = dataReader.GetInt32(0);
                    d.name = dataReader.GetString(1);

                    listToReturn.Add(d);
                }
            }
            return listToReturn;
        }
    }
}
