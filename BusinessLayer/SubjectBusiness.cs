﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer
{
    public class SubjectBusiness : ISubjectBusiness
    {
        private ISubjectRepository subjectRepository;

        public SubjectBusiness(ISubjectRepository subjectRepository)
        {
            this.subjectRepository = subjectRepository;
        }
        public void GetConnectionString(string connString)
        {
            this.subjectRepository.GetConnectionString(connString);
        }
        public List<Subject> GetAllSubjects()
        {
            List<Subject> subjects = this.subjectRepository.GetAllSubjects();
            if (subjects.Count > 0)
            {
                return subjects;
            }
            else
            {
                return null;
            }
        }

        public List<Subject> GetAllSubjectsByDepId(int id)
        {
            return this.subjectRepository.GetAllSubjects().Where(s => s.departmentID == id).ToList();
        }

    }
}
