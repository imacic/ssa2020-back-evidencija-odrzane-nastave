﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

 namespace BusinessLayer
{
    public interface ITeachingRecordsBusiness
    {
        void GetConnectionString(string connString);
        List<TeachingRecords> GetAllTeachingRecords();

        List<TeachingRecordsJoin> GetAllTeachingRecordsJoin();
        List<TeachingRecordsJoin> GetTeachingRecordsById(int LecturerID);
        TeachingRecordsUpdate GetTeachingRecordById(int id);
        bool InsertTeachingRecord(TeachingRecords tr);

        bool UpdateTeachingRecord(TeachingRecords tr);

        bool DeleteTeachingRecord(int id);
    }
}
