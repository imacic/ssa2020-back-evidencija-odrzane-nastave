﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface IClassroomBusiness
    {
        void GetConnectionString(string connString);
        List<Classroom> GetAllClassrooms();
    }
}
