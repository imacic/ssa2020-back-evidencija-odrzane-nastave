﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using DataLayer.Models;

namespace BusinessLayer
{
    public class TeachingRecordsBusiness : ITeachingRecordsBusiness
    {
        private ITeachingRecordsRepository teachingRecordsRepository;
        public TeachingRecordsBusiness(ITeachingRecordsRepository teachingRecordsRepository)
        {
            this.teachingRecordsRepository = teachingRecordsRepository;
        }
        public void GetConnectionString(string connString)
        {
            this.teachingRecordsRepository.GetConnectionString(connString);
        }
        public List<TeachingRecords> GetAllTeachingRecords()
        {
            List<TeachingRecords> teachingRecords = this.teachingRecordsRepository.GetAllTeachingRecords();
            if (teachingRecords.Count > 0)
            {
                return teachingRecords;
            }
            else
            {
                return null;
            }
        }
        public List<TeachingRecordsJoin> GetAllTeachingRecordsJoin()
        {
            List<TeachingRecordsJoin> teachingRecordsJoin = this.teachingRecordsRepository.GetAllTeachingRecordsJoin();
            if (teachingRecordsJoin.Count > 0)
            {
                return teachingRecordsJoin;
            }
            else
            {
                return null;
            }
        }

        public List<TeachingRecordsJoin> GetTeachingRecordsById(int LecturerID)
        {
            return teachingRecordsRepository.GetAllTeachingRecordsJoin().Where(tr => tr.lecturerID == LecturerID).ToList();
        }
        public bool InsertTeachingRecord(TeachingRecords tr)
        {
            return (this.teachingRecordsRepository.InsertTeachingRecord(tr) > 0) ? true : false;
        }
        public bool UpdateTeachingRecord(TeachingRecords tr)
        {
            bool result = false;
            if (tr.id != 0 && this.teachingRecordsRepository.UpdateTeachingRecord(tr) > 0)
            {
                result = true;
            }
            return result;
        }
        public bool DeleteTeachingRecord(int id)
        {
            bool result = false;
            if (id != 0 && this.teachingRecordsRepository.DeleteTeachingRecord(id) > 0)
            {
                result = true;
            }
            return result;
        }
        public TeachingRecordsUpdate GetTeachingRecordById(int id)
        {
            return this.teachingRecordsRepository.GetAllTeachingRecordsUpdate().Where(t => t.id == id).FirstOrDefault();
        }
    }
}
