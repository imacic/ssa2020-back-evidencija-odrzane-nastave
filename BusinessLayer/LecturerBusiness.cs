﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using DataLayer.Models;

namespace BusinessLayer
{
    public class LecturerBusiness : ILecturerBusiness
    {
        private ILecturerRepository lecturerRepository;
        public LecturerBusiness(ILecturerRepository lecturerRepository)
        {
            this.lecturerRepository = lecturerRepository;
        }
        public void GetConnectionString(string connString)
        {
            this.lecturerRepository.GetConnectionString(connString);
        }
        public List<Lecturer> GetAllLecturers()
        {
            List<Lecturer> lecturers = this.lecturerRepository.GetAllLecturers();
            if (lecturers.Count > 0)
            {
                return lecturers;
            }
            else
            {
                return null;
            }
        }
        public List<LecturerJoin> GetAllLecturersJoin()
        {
            List<LecturerJoin> lecturers = this.lecturerRepository.GetAllLecturersJoin();
            if (lecturers.Count > 0)
            {
                return lecturers;
            }
            else
            {
                return null;
            }
        }
        public bool InsertLecturer(Lecturer l)
        {
            return (this.lecturerRepository.InsertLecturer(l) > 0) ? true : false;
        }

        public LecturerJoin GetLecturer(String username)
        {
            return this.lecturerRepository.GetAllLecturersJoin().Where(l => l.username == username).FirstOrDefault();
        }

        public List<Lecturer> GetAllLecturersByDepId(int id)
        {
            return this.lecturerRepository.GetAllLecturers().Where(l => l.departmentID == id).ToList();
        }
    }
}
