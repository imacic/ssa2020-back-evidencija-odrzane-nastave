﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface IDepartmentBusiness
    {
        void GetConnectionString(string connString);
        List<Department> GetAllDepartments();
    }
}
