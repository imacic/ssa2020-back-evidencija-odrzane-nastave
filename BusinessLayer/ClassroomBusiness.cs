﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLayer;
using DataLayer.Models;

namespace BusinessLayer
{
    public class ClassroomBusiness : IClassroomBusiness
    {
        private IClassroomRepository classroomRepository;
        public ClassroomBusiness(IClassroomRepository classroomRepository)
        {
            this.classroomRepository = classroomRepository;
        }
        public void GetConnectionString(string connString)
        {
            this.classroomRepository.GetConnectionString(connString);
        }
        public List<Classroom> GetAllClassrooms()
        {
            List<Classroom> classrooms = this.classroomRepository.GetAllClassrooms();
            if (classrooms.Count > 0)
            {
                return classrooms;
            }
            else
            {
                return null;
            }
        }

    }
}
