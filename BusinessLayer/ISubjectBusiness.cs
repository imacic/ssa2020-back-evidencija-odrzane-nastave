﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface ISubjectBusiness
    {
        void GetConnectionString(string connString);
        List<Subject> GetAllSubjects();
        List<Subject> GetAllSubjectsByDepId(int id);
    }
}
