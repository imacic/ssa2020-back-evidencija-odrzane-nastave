﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface ILecturerBusiness
    {
        void GetConnectionString(string connString);
        List<Lecturer> GetAllLecturers();
        List<LecturerJoin> GetAllLecturersJoin();
        LecturerJoin GetLecturer(String username);
        bool InsertLecturer(Lecturer l);
        List<Lecturer> GetAllLecturersByDepId(int id);
    }
}
