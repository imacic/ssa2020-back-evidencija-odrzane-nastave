﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLayer;
using DataLayer.Models;

namespace BusinessLayer
{
    public class DepartmentBusiness : IDepartmentBusiness
    {
        private IDepartmentRepository departmentRepository;
        public DepartmentBusiness(IDepartmentRepository departmentRepository)
        {
            this.departmentRepository = departmentRepository;
        }
        public void GetConnectionString(string connString)
        {
            this.departmentRepository.GetConnectionString(connString);
        }
        public List<Department> GetAllDepartments()
        {
            List<Department> departments = this.departmentRepository.GetAllDepartments();
            if(departments.Count > 0)
            {
                return departments;
            }
            else
            {
                return null;
            }
        }

    }
}
