using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc.Filters;
using BusinessLayer;
using DataLayer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace EvidencijaNastave
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public static string ConnectionString { get; set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddRazorPages();
            services.AddScoped<IDepartmentBusiness, DepartmentBusiness>();
            services.AddScoped<IDepartmentRepository, DepartmentRepository>();
            services.AddScoped<IClassroomBusiness, ClassroomBusiness>();
            services.AddScoped<IClassroomRepository, ClassroomRepository>();
            services.AddScoped<ILecturerBusiness, LecturerBusiness>();
            services.AddScoped<ILecturerRepository, LecturerRepository>();
            services.AddScoped<ISubjectBusiness, SubjectBusiness>();
            services.AddScoped<ISubjectRepository, SubjectRepository>();
            services.AddScoped<ITeachingRecordsBusiness, TeachingRecordsBusiness>();
            services.AddScoped<ITeachingRecordsRepository, TeachingRecordsRepository>();

            services.AddCors();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            ConnectionString = Configuration["ConnectionString"];

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors(builder =>
            builder.WithOrigins("http://localhost:4200")
            .AllowAnyHeader()
            .AllowAnyMethod());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });
        }

    }
}
