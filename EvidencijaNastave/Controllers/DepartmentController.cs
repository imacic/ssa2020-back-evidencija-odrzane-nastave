﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using DataLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EvidencijaNastave.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {

        private readonly IDepartmentBusiness departmentBusiness;

        public DepartmentController(IDepartmentBusiness departmentBusiness)
        {
            this.departmentBusiness = departmentBusiness;
            this.departmentBusiness.GetConnectionString(Startup.ConnectionString);
        }

        // GET: api/Department
        [HttpGet]
        public List<Department> GetAllDepartments()
        {
            return this.departmentBusiness.GetAllDepartments();
        }

        /*
        // GET: api/Department/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        */

        // POST: api/Department
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Department/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
