﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using DataLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EvidencijaNastave.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectController : ControllerBase
    {
        private readonly ISubjectBusiness subjectBusiness;

        public SubjectController(ISubjectBusiness subjectBusiness)
        {
            this.subjectBusiness = subjectBusiness;
            this.subjectBusiness.GetConnectionString(Startup.ConnectionString);
        }
        // GET: api/Subject
        [HttpGet]
        public List<Subject> GetAllSubjects()
        {
            return this.subjectBusiness.GetAllSubjects();
        }

        // GET: api/Subject/1
        [Route("{id}")]
        [HttpGet]
        public List<Subject> GetAllSubjectsByDepId(int id)
        {
            return this.subjectBusiness.GetAllSubjectsByDepId(id);
        }

        /*
        // GET: api/Subject/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        */

        // POST: api/Subject
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Subject/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
