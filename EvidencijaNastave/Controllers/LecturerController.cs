﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using DataLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EvidencijaNastave.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturerController : ControllerBase
    {
        private readonly ILecturerBusiness lecturerBusiness;

        public LecturerController(ILecturerBusiness lecturerBusiness)
        {
            this.lecturerBusiness = lecturerBusiness;
            this.lecturerBusiness.GetConnectionString(Startup.ConnectionString);
        }

        // GET: api/Lecturer
        [HttpGet]
        public List<Lecturer> GetAllLecturers()
        {
            return this.lecturerBusiness.GetAllLecturers();
        }
        // GET: api/Lecturer/join
        [Route("join")]
        [HttpGet]
        public List<LecturerJoin> GetAllLecturersJoin()
        {
            return this.lecturerBusiness.GetAllLecturersJoin();
        }

        // GET: api/Lecturer/1
        [Route("{id}")]
        [HttpGet]
        public List<Lecturer> GetAllLecturersByDepId(int id)
        {
            return this.lecturerBusiness.GetAllLecturersByDepId(id);
        }

        /*
        // GET: api/Lecturer/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        */

        // POST: api/Lecturer/insert
        [Route("insert")]
        [HttpPost]
        public IActionResult InsertLecturer([FromBody]Lecturer l)
        {
            try
            {
                var insert = this.lecturerBusiness.InsertLecturer(l);
                return Ok(insert);
            }
            catch(ApplicationException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // POST: api/Lecturer/login
        [Route("login")]
        [HttpPost]
        public IActionResult LoginLecturer([FromBody]Lecturer l)
        {
            try
            {
                List<Lecturer> lista= lecturerBusiness.GetAllLecturers();
                if (string.IsNullOrWhiteSpace(l.password))
                    throw new ApplicationException("Password is required");

                else if (string.IsNullOrWhiteSpace(l.username))
                    throw new ApplicationException("Username is required");

                for (int i = 0; i < lista.Count; i++)
                {
                    if (lista[i].username == l.username)
                    {
                        if (lista[i].password == l.password)
                        {
                            return Ok(lecturerBusiness.GetLecturer(l.username));
                        }
                    }
                }
                    throw new ApplicationException("Username or password is incorrect.");
            }
            catch (ApplicationException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // PUT: api/Lecturer/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
