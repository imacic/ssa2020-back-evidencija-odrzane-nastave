﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using DataLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace EvidencijaNastave.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeachingRecordsController : ControllerBase
    {

        private readonly ITeachingRecordsBusiness teachingRecordsBusiness;


        public TeachingRecordsController(ITeachingRecordsBusiness teachingRecordsBusiness)
        {
            this.teachingRecordsBusiness = teachingRecordsBusiness;
            this.teachingRecordsBusiness.GetConnectionString(Startup.ConnectionString);
        }

        // GET: api/TeachingRecords
        [HttpGet]
        public List<TeachingRecords> GetAllTeachingRecords()
        {
            return this.teachingRecordsBusiness.GetAllTeachingRecords();
        }
        // GET: api/TeachingRecords/join
        [Route("join")]
        [HttpGet]
        public List<TeachingRecordsJoin> GetAllTeachingRecordsJoin()
        {
            return this.teachingRecordsBusiness.GetAllTeachingRecordsJoin();
        }

        // GET api/TeachingRecords/1
        [Route("records/{id}")]
        [HttpGet]
        public List<TeachingRecordsJoin> GetRecordsById(int id)
        {
            return this.teachingRecordsBusiness.GetTeachingRecordsById(id);
        }

        // POST: api/TeachingRecords/records
        [Route("records")]
        [HttpPost]
        public List<TeachingRecordsJoin> GetRecords([FromBody]Lecturer l)
        {
            return this.teachingRecordsBusiness.GetTeachingRecordsById(l.id);
        }

        // POST: api/TeachingRecords/insert
        [Route("insert")]
        [HttpPost]
        public bool InsertRecord([FromBody]TeachingRecords tr)
        {
            return this.teachingRecordsBusiness.InsertTeachingRecord(tr);
        }

        /*
        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        */

        // POST api/TeachingRecords
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        //POST api/TeachingRecords/1
        [Route("{id}")]
        [HttpGet]
        public TeachingRecordsUpdate GetTeachingRecordById(int id)
        {
            return this.teachingRecordsBusiness.GetTeachingRecordById(id);
        }

        // PUT: api/TeachingRecords/update
        [Route("update")]
        [HttpPut]
        public bool UpdateTeachingRecord([FromBody] TeachingRecords tr)
        {
            return this.teachingRecordsBusiness.UpdateTeachingRecord(tr);
        }

        // DELETE: api/TeachingRecords/5/delete
        [HttpDelete("{id}/delete")]
        public bool DeleteTeachingRecord(int id)
        {
            return this.teachingRecordsBusiness.DeleteTeachingRecord(id);
        }
    }
}
