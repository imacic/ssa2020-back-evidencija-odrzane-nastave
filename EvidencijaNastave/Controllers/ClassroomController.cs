﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using DataLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EvidencijaNastave.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassroomController : ControllerBase
    {
        private readonly IClassroomBusiness classroomBusiness;

        public ClassroomController(IClassroomBusiness classroomBusiness)
        {
            this.classroomBusiness = classroomBusiness;
            this.classroomBusiness.GetConnectionString(Startup.ConnectionString);
        }
        // GET: api/Classroom
        [HttpGet]
        public List<Classroom> GetAllClassrooms()
        {
            return this.classroomBusiness.GetAllClassrooms();
        }

        /*
        // GET: api/Classroom/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        */

        // POST: api/Classroom
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Classroom/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
